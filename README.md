# README #

This project collects Peptides scan server from submodules ([frontend](https://bitbucket.org/Bedzior/engpeptidesfront) and [backend](https://bitbucket.org/Bedzior/engpeptidesbackend)) and configures as one.

Pipelines use a Docker image that is built on [this repository's](https://bitbucket.org/Bedzior/docker-java8-chrome-xvfb) update.

### Downloading ###
Just fetch this repository with
```
#!bash
git clone https://Bedzior@bitbucket.org/Bedzior/peptides.git
```
then download the submodules:
```
#!bash
cd peptides
git submodule update --init
```
## Initial run ##
In order to have the necessary jar files placed in local maven repository, run:
```
#!bash
mvn clean validate
```

| Running | Continuous integration |
|---------|------------------------|
| Having fetched this project's contents, you're up to go | In case of a windowless environment, maven should be run with an option disabling all windowed tests |
| `mvn clean install` | `mvn clean install -P no-gui` |