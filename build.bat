@ECHO off
SET serverDirectory=%1
SET mavenOpts=%2
IF "%serverDirectory%"=="" (
	ECHO No server directory defined ^(1st parameter^) 
	EXIT /B 1
)
IF "%mavenOpts%"=="" (
	ECHO No maven options defined ^(2nd parameter^)
)
mvn clean install -Dmaven.test.skip %mavenOpts%
COPY /Y Frontend\target\frontend-1.0.0-SNAPSHOT.war "%serverDirectory%\ROOT.war"
COPY /Y Backend\target\api.war "%serverDirectory%"
